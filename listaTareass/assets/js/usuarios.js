$(document).on("ready", main);

function main(){
		mostrarDatos("");
		$("#buscar").keyup(function(){
			buscar = $("#buscar").val();
			mostrarDatos(buscar);
		});
		$("#btnbuscar").click(function(){
			mostrarDatos("");
			$("#buscar").val("");
		});
	$("#msg-error").hide();
	$("#form-create-usuario").submit(function(event){
		event.preventDefault();
		$.ajax({
			url:$(this).attr('action'),
			type:$(this).attr('method'),
			data:$(this).serialize(),
			success:function(resp){
				if (resp==="Exito") {
					alert(resp);
					//mostrarDatos("");
					$("#msg-error").hide();
					$("#form-create-usuario")[0].reset();
				}else if(resp==="Error"){
					alert(resp);
				}
				else{
					$(".list-errors").html(resp);
					$("#msg-error").show();
				}
			}

		});

	});
	$("body").on("click","#listaEmpleados button",function(event){
		idsele = $(this).attr("value");
		eliminar(idsele);
	});


}


function mostrarDatos(valor){
	$.ajax({
		url:"http://localhost/empresa/usuarios/mostrar",
		type:"POST",
		data:{buscar:valor},
		success:function(resp){
			//alert(respuesta);
			var registros = eval(resp);

			html ="<table class='table table-responsive table-bordered'><thead>";
 			html +="<tr><th>#</th><th>Nombre Usuario</th><th>Apellido</th><th>Email</th><th>Accion</th></tr>";
			html +="</thead><tbody>";
			for (var i = 0; i < registros.length; i++) {
				html +="<tr><td>"+registros[i]["id_usuario"]+"</td><td>"+registros[i]["nombres"]+"</td><td>"+registros[i]["apellidos"]+"</td><td>"+registros[i]["email"]+"</td><td>"+"' <button class='btn btn-warning' data-toggle='modal' data-target='#myModal'>Editar</a> <button class='btn btn-danger' type='button' value='"+registros[i]["id_usuario"]+"'>X</a> </button></td></tr>";
			};
			html +="</tbody></table>";
			$("#listaEmpleados").html(html);
		}
	});
}

function eliminar(idsele){
	$.ajax({
		url:"http://localhost/empresa/usuarios/eliminar",
		type:"POST",
		data:{id:idsele},
		success:function(resp){
			alert(resp);
			mostrarDatos("");
		}
	});
}
