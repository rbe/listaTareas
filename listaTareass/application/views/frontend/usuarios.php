<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Proyecto Tareas | Usuarios</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<!--css del interfaz usuarios en esta direccion-->
	<link rel="stylesheet" href="assets/css/estilosPantallaConsultor.css" />

</head>
<body>


	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <!--  <span class="sr-only">Toggle navigation</span>-->
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Proyectos</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?= base_url('empleados')?>">Tareas</a></li>
          	<li><a href="<?= base_url('usuarios')?>">Usuarios</a></li>

          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user fa-fw"></i> <?= $this->session->userdata('login')?><b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#"><i class="fa fa-user fa-fw"></i>Perfil</a></li>
                  <li><a href="javascript:void(0)" id="cerrar"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a></li>
                </ul>
              </li>
            </ul>
        </div>
      </div>
    </nav>

    <div class="container">

		<section class="contenido">
			<div class="row">

				<ul class="nav nav-tabs">
			        <li class="active"><a href="#tab1" data-toggle="tab">Registrar</a></li>
			        <li><a id="tab-consultar" href="#tab2" data-toggle="tab">Empleados</a></li>
			    </ul>
          <!--registro de usuarios nuevos-->
			    <div class="tab-content">
			        <div class="tab-pane  active" id="tab1">
			        	<div class="col-lg-4"></div>
			            <div class="col-lg-4 text-center">
			            	<h2>Registro de Usuario</h2>
			            	<div class="alert alert-danger" id="msg-error" style="text-align:left;">
                 <!--errores al no completar el formulario-->
							  	<strong>¡Importante!</strong> Corregir los siguientes errores.
							  	<ul class="list-errors"></ul>
							</div>
							<form id="form-create-usuario" style="padding:0px 15px;"class="form-horizontal" role="form" action="<?php base_url();?>usuarios/guardar" method="POST">
		            			<div class="form-group">
		            				<input type="text" name="nombres" class="form-control" placeholder="Ingrese su Nombres"/>
		            			</div>
		            			<div class="form-group">
		            				<input type="text" name="apellidos" class="form-control" placeholder="Ingrese sus Apellidos  "/>
		            			</div>
		            			<div class="form-group">
		            				<input type="email" name="email" class="form-control" placeholder="Ingrese su Email"/>
		            			</div>
		            			<div class="form-group">
		            				<input type="password" name="password" class="form-control" placeholder="Ingrese su Contraseña"/>
		            			</div>
		            			<div class="form-group">
				            		<button type="submit" class="btn btn-primary btn-block" value="Registrar">Registrar</button>
			      				</div>
		            		</form>
		            	</div>
			        </div>

			        <div class="tab-pane fade" id="tab2">

			            <div class="row">
			            	<br>
			            	<div class="col-lg-7"></div>
			            	<div class="col-lg-3">
			            		<input type="text" class="form-control" id="buscar" placeholder="Buscar">
			            	</div>
			            	<div class="col-lg-2">
			            		<input type="button" class="btn btn-primary btn-block" id="btnbuscar" value="Mostrar Todo" data-toggle='modal' data-target='#basicModal'>
			            	</div>
			            </div>
			            <hr>
			            <div class="row">
			            	<div id="listaEmpleados" class="col-lg-8">

			            	</div>
			            	<div class="col-lg-4">
			            		<div class="panel panel-default">


			            	</div>

			            </div>

			        </div>

			    </div>


			</div>

		</section>


    </div>


<script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/usuarios.js"></script>
<script src="<?php echo base_url();?>assets/js/login.js"></script>
</body>
</html>
</body>
</html>
