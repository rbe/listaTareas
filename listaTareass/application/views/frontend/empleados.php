<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Proyecto Tareas</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<!--css de la interfaz del consultor-->
	<link rel="stylesheet" href="assets/css/estilosPantallaConsultor.css" />

</head>
<body>


	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Proyectos</a>
        </div>
				<!--eleccion de dos ventanas, tares y sobre usuarios-->
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?= base_url('empleados')?>">Tareas</a></li>
          	<li><a href="<?= base_url('usuarios')?>">Usuarios</a></li>
					</ul>
          <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user fa-fw"></i> <?= $this->session->userdata('name')?><b class="caret"></b>
                </a>
                <ul class="dropdown-menu">

                  <li><a href="javascript:void(0)" id="cerrar"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a></li>
                </ul>
              </li>
            </ul>
        </div>
      </div>
    </nav>

    <div class="container">
    <!--dos pestañas, registrar tareas y mis tareas que muestra las tareas registradas-->
		<section class="contenido">
			<div class="row">

				<ul class="nav nav-tabs">
			        <li class="active"><a href="#tab1" data-toggle="tab">Registrar Tareas</a></li>
			        <li><a id="tab-consultar" href="#tab2" data-toggle="tab">Mis Tareas</a></li>
			    </ul>

			    <div class="tab-content">
			        <div class="tab-pane  active" id="tab1">
			        	<div class="col-lg-4"></div>
			            <div class="col-lg-4 text-center">
			            	<h2>Registro de Tarea</h2>
							<form id="form-create-empleado" class="form-horizontal" role="form" action="<?php base_url();?>empleados/guardar" method="POST">
		            			<div class="form-group">
		            				<input type="text" name="nombresProyecto" class="form-control" placeholder="Ingrese Nombre de proyecto"/>
		            			</div>
		            			<div class="form-group">
		            				<input type="text" name="nombreTarea" class="form-control" placeholder="Ingrese Tarea  "/>
		            			</div>
		            			<div class="form-group">
		            				<input type="text" name="comentario" class="form-control" placeholder="Comentarios"/>
		            			</div>

		            			<div class="form-group">
				            		<button type="submit" class="btn btn-primary btn-block" value="Registrar">Registrar</button>
			      				</div>
		            		</form>
		            	</div>
			        </div>
              <!--muestra la lista de tareas en una tabla a traves de empleado.js-->
			        <div class="tab-pane fade" id="tab2">

			            <div class="row">
			            	<br>
			            	<div class="col-lg-7"></div>
			            	<div class="col-lg-3">
			            		<input type="text" class="form-control" id="buscar" placeholder="Buscar">
			            	</div>
			            	<div class="col-lg-2">
			            		<input type="button" class="btn btn-primary btn-block" id="btnbuscar" value="Mostrar Todo" data-toggle='modal' data-target='#basicModal'>
			            	</div>
			            </div>
			            <hr>
			            <div class="row">
			            	<div id="listaTareas" class="col-lg-8">

			            	</div>
			            	<div class="col-lg-4">
			            		<div class="panel panel-default">
												<!--permita que al hacer click en editar, los datos se dirijan al siguiente formularios-->
 									<div class="panel-heading">Editar Tarea</div>
								  	<div class="panel-body">
								  		<form id="form-actualizar" class="form-horizontal" action="<?php echo base_url();?>empleados/actualizar" method="post" role="form" style="padding:0 10px;">
								  			<div class="form-group">
								  				<label>Nombre Proyecto:</label>
								  				<input type="hidden" id="idsele" name="idsele" value="">
								  				<input type="text" name="nombressele" id="nombressele" class="form-control">
								  			</div>
								  			<div class="form-group">
								  				<label>Nombre Tarea:</label>
								  				<input type="text" name="tareasele" id="tareasele" class="form-control">
								  			</div>
								  			<div class="form-group">
								  				<label>Comentario:</label>
								  				<input type="text" name="comentariosele" id="comentariosele" class="form-control">
								  			</div>
								  			<div class="form-group">
								  				<button type="button" id="btnactualizar" class="btn btn-success btn-block">Actualizar</button>

								  			</div>
								  		</form>

								  	</div>
								</div>

			            	</div>

			            </div>

			        </div>

			    </div>


			</div>

		</section>


    </div>


<script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/empleado.js"></script>
<script src="<?php echo base_url();?>assets/js/login.js"></script>
</body>
</html>
</body>
</html>
