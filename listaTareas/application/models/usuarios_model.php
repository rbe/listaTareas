<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	function mostrar($valor){
		/*$this->db->like("nombres_empleado",$valor);
		$this->db->or_like('apellidos_empleado', $valor);
		$consulta = $this->db->get("empleados");
		return $consulta->result();*/

		$this->db->select('*');
		$this->db->from('usuarios');
//		$this->db->join('usuarios', 'usuarios.id_usuario = empleados.id_usuario');
		$this->db->like("usuarios.nombres",$valor);
		$this->db->or_like('usuarios.apellidos', $valor);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	function guardar($data){

		$this->db->insert("usuarios", $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}

	}

	function eliminar($id){
		$this->db->where('id_usuario', $id);
		$this->db->delete('usuarios');
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}
}
